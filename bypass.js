function confirmRefresh() {
    return confirm("Are you sure you want to refresh?");
  }
  
  // Get refresh button by id
  const refreshButton = document.getElementById("refreshButton");
  
  refreshButton.addEventListener("click", () => {
    if (!confirmRefresh()) {
      event.preventDefault();
    }
  });
  
  window.addEventListener("beforeunload", (event) => {
    if (!confirmRefresh()) {
      event.preventDefault();
      event.returnValue = ""; 
    }
  });
  